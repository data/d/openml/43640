# OpenML dataset: Highway-Rail-Grade-Cross-Accidents-1975---2016-USA

https://www.openml.org/d/43640

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Interesting data about accidents with cars on railroad crossing.
Content
This data covering the period from 1975 to 2016. It has a lot of info about every single accident like time, date, coordinates, victims, railroad owner, city, state and etc. 229 665 records.
Acknowledgements
Department of Transportation of the USA made this dataset,
Inspiration
It's interesting to discover the answers with ML methods. Also heatmaps, plots.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43640) of an [OpenML dataset](https://www.openml.org/d/43640). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43640/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43640/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43640/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

